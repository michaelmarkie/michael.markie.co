==================================================================
https://keybase.io/michaelmarkie
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://michael.markie.co
  * I am michaelmarkie (https://keybase.io/michaelmarkie) on keybase.
  * I have a public key ASD_d0rwqu7-D7tgdguhOLcF4SYW7CIcO9XY_QVjw46x7go

To do so, I am signing this object:

{
    "body": {
        "key": {
            "eldest_kid": "0120ff774af0aaeefe0fbb60760ba138b705e12616ec221c3bd5d8fd0563c38eb1ee0a",
            "host": "keybase.io",
            "kid": "0120ff774af0aaeefe0fbb60760ba138b705e12616ec221c3bd5d8fd0563c38eb1ee0a",
            "uid": "e633b44633c9b380e530dc4ad97eeb19",
            "username": "michaelmarkie"
        },
        "service": {
            "hostname": "michael.markie.co",
            "protocol": "https:"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "client": {
        "name": "keybase.io go client",
        "version": "1.0.20"
    },
    "ctime": 1491843365,
    "expire_in": 504576000,
    "merkle_root": {
        "ctime": 1491843339,
        "hash": "6792617b605e885f4695314e552a422ad2a4c8ce00e30e7b83a56c435410cfa40c64f2d33d5071058a9970a41073c583fe356896c6baafd5e19d9d1417d2c4a2",
        "seqno": 1010492
    },
    "prev": "0ac54262ad1c2e39f33906f25917184452d0458b2f1258c9e5d454373e94cd64",
    "seqno": 8,
    "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg/3dK8Kru/g+7YHYLoTi3BeEmFuwiHDvV2P0FY8OOse4Kp3BheWxvYWTFAv97ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwZmY3NzRhZjBhYWVlZmUwZmJiNjA3NjBiYTEzOGI3MDVlMTI2MTZlYzIyMWMzYmQ1ZDhmZDA1NjNjMzhlYjFlZTBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwZmY3NzRhZjBhYWVlZmUwZmJiNjA3NjBiYTEzOGI3MDVlMTI2MTZlYzIyMWMzYmQ1ZDhmZDA1NjNjMzhlYjFlZTBhIiwidWlkIjoiZTYzM2I0NDYzM2M5YjM4MGU1MzBkYzRhZDk3ZWViMTkiLCJ1c2VybmFtZSI6Im1pY2hhZWxtYXJraWUifSwic2VydmljZSI6eyJob3N0bmFtZSI6Im1pY2hhZWwubWFya2llLmNvIiwicHJvdG9jb2wiOiJodHRwczoifSwidHlwZSI6IndlYl9zZXJ2aWNlX2JpbmRpbmciLCJ2ZXJzaW9uIjoxfSwiY2xpZW50Ijp7Im5hbWUiOiJrZXliYXNlLmlvIGdvIGNsaWVudCIsInZlcnNpb24iOiIxLjAuMjAifSwiY3RpbWUiOjE0OTE4NDMzNjUsImV4cGlyZV9pbiI6NTA0NTc2MDAwLCJtZXJrbGVfcm9vdCI6eyJjdGltZSI6MTQ5MTg0MzMzOSwiaGFzaCI6IjY3OTI2MTdiNjA1ZTg4NWY0Njk1MzE0ZTU1MmE0MjJhZDJhNGM4Y2UwMGUzMGU3YjgzYTU2YzQzNTQxMGNmYTQwYzY0ZjJkMzNkNTA3MTA1OGE5OTcwYTQxMDczYzU4M2ZlMzU2ODk2YzZiYWFmZDVlMTlkOWQxNDE3ZDJjNGEyIiwic2Vxbm8iOjEwMTA0OTJ9LCJwcmV2IjoiMGFjNTQyNjJhZDFjMmUzOWYzMzkwNmYyNTkxNzE4NDQ1MmQwNDU4YjJmMTI1OGM5ZTVkNDU0MzczZTk0Y2Q2NCIsInNlcW5vIjo4LCJ0YWciOiJzaWduYXR1cmUifaNzaWfEQLe6AzThq6c7LsY4qhLLxNJsiA2rBSEm4pVNQLVlNXnKQJ3wj0PliNpxY1fvpWcFGqx/NyWi75JJwLXhXg/SIw2oc2lnX3R5cGUgpGhhc2iCpHR5cGUIpXZhbHVlxCAziV3CyL8fG3zIreh+CwgOv5to6Am+6jDDtXwFJiu2NKN0YWfNAgKndmVyc2lvbgE=

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/michaelmarkie

==================================================================