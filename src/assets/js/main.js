var eachUb8v6 = function (callback) {
    var v, c = 0, len = (typeof(this)=='object'?Object.getOwnPropertyNames(this):this).length;
    for (var i in this)
        if (c++ < len)
            callback.apply(v = this[i], [i, v]);
};

Object.defineProperty(Object.prototype, 'each', { value: eachUb8v6, enumberable: false });
HTMLCollection.prototype.each = NodeList.prototype.each = Array.prototype.each = eachUb8v6;

String.prototype.find_replace = function(find_replace) {
	var
		str = this,
		len = find_replace.length || Object.getOwnPropertyNames(find_replace).length,
		c = 0;
	for (var find in find_replace) {
		if (c < len)
			str = str.replace(new RegExp(find, 'g'), find_replace[find]);
		c++;
	}
	return str + '';
};

(function(){

	window.addEventListener('load', function () {
		/*
		For the purpose of avoiding spam lets change the email address to my actual email address.
		*/
		document.querySelectorAll('.email').each( function () {

			var html = this.innerHTML;

			this.innerHTML = html.find_replace({
				'sp.': '',
				'.am': ''
			});

		});





		/*
		Lightbox
		*/
		
		var lbox;

		if (lbox = document.querySelector('#lightbox')) {

			lbox.addEventListener('click', function () {

				lbox.style.display = 'none';
				lbox.querySelector('div').innerHTML = '';

			});


			document.querySelector('.gallery').querySelectorAll('a').each(function () {

				this.addEventListener('click', function( event ) {

					if ((window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) > 620) {

						lbox.style.display = 'flex';
						lbox.querySelector('div').innerHTML = this.querySelector('.lightbox').outerHTML;

						event.preventDefault();
						return false;
					}

				});
			})
		}
	});

})();