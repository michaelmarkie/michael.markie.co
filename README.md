# michael.markie.co
## Web based Resume/CV

### Requirements:

-   Node 10+

-   NPM 6+


### Building:

```bash
npm install
npm run serve
```

May take some time to build!
After which go to localhost:4000
