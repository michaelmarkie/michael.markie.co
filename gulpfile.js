const { src, dest, series, parallel, watch } = require('gulp');
const pug = 			require('gulp-pug');
const sass = 			require('gulp-sass');
const rename = 			require("gulp-rename");
const resizer = 		require('gulp-images-resizer');
const imagemin = 		require("gulp-imagemin");
const webp = 			require("imagemin-webp");
const extReplace = 		require("gulp-ext-replace");
const connect = 		require("gulp-connect");
const iconfont = 		require("gulp-iconfont");
const iconfontCss = 	require('gulp-iconfont-css');


/* PUG */
function pugTask(cb) {
	src('src/**/*.pug')
		.pipe(pug({ pretty: true }))
		.pipe(dest('public'))
		.on('finish', cb);
}

function watchPugTask(cb) {
	watch('src/**/*.pug', pugTask);

	return cb();
}

/* SASS */
function sassTask(cb) {
    src('src/assets/sass/**/*.sass', { sourcemaps: true })
        .pipe(sass().on('error', console.error))
		.pipe(dest('public/assets/css',{ sourcemaps: '.' }))
		.on('finish', cb);
}

function watchSassTask(cb) {
	watch('src/assets/sass/**/*.sass', sassTask);

	return cb();
}

/* ICONS */
function iconTask(cb) {

	var fontName = 'michael.markie.co';

	src('src/assets/images/icons/**/*.svg')
		.pipe(imagemin([
			imagemin.svgo({
				plugins: [
					{
						cleanupNumericValues: 0
					}
				]
			})
		]))
		.pipe(iconfontCss({
			fontName: fontName,
			path: 'src/assets/sass/_icons.sass',
			targetPath: '../sass/icons.sass',
			fontPath: '../fonts/'
		}))
		.pipe(iconfont({
			  fontName: fontName
		}))
		.pipe(dest('src/assets/fonts/'))
		.on('finish', cb);
}

/* GALLERY */
function galleryTask(cb) {
	const sizes = [290,842];
	let count = 0;

	sizes.forEach(function(size){

		src('src/assets/gallery/*.{jpg, jpeg, png}')
			.pipe(resizer({
				format: 'jpg',
				height: size
			}))
			.pipe(rename({
				prefix: 'auto-' + size + '_'
			}))
			.pipe(dest('public/assets/gallery'))
			.pipe(imagemin([
				webp({
					quality: 90
				})
			]))
			.pipe(extReplace('.webp'))
			.pipe(dest('public/assets/gallery'))
			.on('finish', function () {
				count += 1;
				if (sizes.length == count)
					cb();
			});
	})

}

/* COPY */
function copyTask(cb) {
	src('src/**/*.{txt,png,jpg,jpeg,woff,woff2,ttf,eot,js}')
		.pipe(dest('public'))
		.on('finish', cb);
}

function watchCopyTask(cb) {
	watch('src/**/*.{txt,png,jpg,jpeg,woff,woff2,ttf,eot,js}', copyTask);

	return cb();
}

/* SERVE */
function serveTask(cb) {
	connect.server({
		livereload: true,
		root: 'public',
		//host: '10.0.0.106',
		port: 4000
	});
}



/* EXPORTS */

exports.build = series(iconTask, parallel(sassTask, pugTask, copyTask, galleryTask));
exports.watch = parallel(watchSassTask, watchPugTask, watchCopyTask);

exports.serve = series(exports.build, parallel(serveTask, exports.watch));

exports.default = series(pugTask, sassTask);